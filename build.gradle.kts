import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.31"
    application
}

repositories {
    mavenCentral()
    jcenter()
}

application {
    mainClass.set("main.kotlin.ProxyServerKt")
}
dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.4.3")
    implementation("io.ktor:ktor-server-core:1.5.2")
    implementation("io.ktor:ktor-client-core:1.5.2")
    implementation("io.ktor:ktor-server-cio:1.5.2")
    implementation("io.ktor:ktor-client-cio:1.5.2")
    implementation("ch.qos.logback:logback-classic:1.2.3")
}
val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "1.8"
}
val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "1.8"
}