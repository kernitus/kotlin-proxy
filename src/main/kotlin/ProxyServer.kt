package main.kotlin

import io.ktor.application.*
import io.ktor.client.*
import io.ktor.client.features.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.network.selector.*
import io.ktor.network.sockets.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.cio.*
import io.ktor.server.engine.*
import io.ktor.util.*
import io.ktor.utils.io.*
import kotlinx.coroutines.*
import java.io.*
import java.net.*

private const val PROXY_PORT = 8989
private val cache: MutableMap<String, HttpResponse> = mutableMapOf()

fun main() {
    embeddedServer(CIO, port = PROXY_PORT) {
        intercept(ApplicationCallPipeline.Call) {
            println("${call.request.httpMethod.value} on ${call.request.uri}")
            val request = call.request

            // Make the HTTP request on behalf of the client
            var response: HttpResponse? = null
            try {
                response = sendRequest(request.httpMethod, request.uri, request.headers, request.receiveChannel())
                if (request.httpMethod == HttpMethod.Get) {
                    println("Caching response")
                    cache[request.uri] = response
                }
            } catch (e: RedirectResponseException) {
                if (e.response.status == HttpStatusCode.NotModified) {
                    println("Using cached response")
                    response = cache[request.uri]
                }
            }

            if (response == null) {
                println("Empty response, sending 404")
                call.respond(HttpStatusCode.NotFound)
            } else {
                answerRequest(response, call)
            }
        }
    }.start(wait = true)
}


suspend fun sendRequest(httpMethod: HttpMethod, url: String, requestHeaders: Headers, requestBody: Any): HttpResponse =
    HttpClient().request {
        method = httpMethod
        url(url)
        headers.appendAll(requestHeaders)
        body = requestBody
    }

suspend fun answerRequest(response: HttpResponse, call: ApplicationCall) {
    val proxiedHeaders = response.headers
    val contentType = proxiedHeaders[HttpHeaders.ContentType]
    val contentLength = proxiedHeaders[HttpHeaders.ContentLength]

    call.respond(object : OutgoingContent.WriteChannelContent() {
        override val contentLength: Long? = contentLength?.toLong()
        override val contentType: ContentType? = contentType?.let { ContentType.parse(it) }
        override val headers: Headers = Headers.build {
            appendAll(proxiedHeaders.filter { key, _ ->
                !key.equals(HttpHeaders.ContentType, true)
                        && !key.equals(HttpHeaders.ContentLength, true)
            })
        }
        override val status: HttpStatusCode = response.status
        override suspend fun writeTo(channel: ByteWriteChannel) {
            response.content.copyAndClose(channel)
        }
    })
}
